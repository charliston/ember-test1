# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Dashboard CMS login ###
<http://localhost:3000/admin>  
**Email**: admin@example.com  
**Password**: password
  
  
### API documentation ###
<http://localhost:3000/documentation>

## Dependencias ##
Ember-cli

```
#!sh

$ npm install -g ember-cli
$ npm install -g broccoli-ember-hbs-template-compiler
```

## Instalações ##
Em /api : 
```
#!sh
$ bundle exec spring binstub --all
$ bundle exec rake db:create db:migrate db:seed
```
Em /front-end:

```
#!sh
$ bower install
```
## Rodar Ember.js ##
Em /api : 
```
#!sh
$ bundle exec rails server
```
Em /front-end:

```
#!sh
$ ember server
```
** Deixar os dois serviços rodando **  

Acessar em <http://localhost:4200>