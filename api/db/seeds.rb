Contact.create!([
  {first_name: "Laurence", last_name: "Sawayn", email: "alexandrea_stokes@mayer.org", title: "Legacy Optimization Designer"},
  {first_name: "Duane", last_name: "Mueller", email: "beatrice.bahringer@okuneva.info", title: "Product Configuration Developer"},
  {first_name: "Jaiden", last_name: "Waters", email: "ahmad.buckridge@thompsonhilpert.name", title: "Global Tactics Coordinator"},
  {first_name: "Darwin", last_name: "Homenick", email: "gabrielle@windler.biz", title: "Senior Applications Consultant"},
  {first_name: "Camryn", last_name: "Heller", email: "wilfrid@swaniawski.biz", title: "Product Branding Supervisor"},
  {first_name: "Lon", last_name: "Bernhard", email: "damon.hansen@berge.net", title: "International Quality Manager"},
  {first_name: "Cedrick", last_name: "Fisher", email: "margot.haag@hamillhilll.biz", title: "Dynamic Usability Manager"},
  {first_name: "Marianne", last_name: "O'Conner", email: "alfred@kilbackparisian.com", title: "Corporate Tactics Technician"},
  {first_name: "Brook", last_name: "Feest", email: "desmond_schaden@bogan.com", title: "Global Intranet Orchestrator"},
  {first_name: "Tillman", last_name: "Keeling", email: "cleta.sipes@quitzon.org", title: "Dynamic Web Developer"}
])
