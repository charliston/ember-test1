/* global moment:true */

import Ember from 'ember';

export default Ember.Handlebars.makeBoundHelper(function(date, format) {
  return moment(date).format(format);
});

/*
import Ember from 'ember';

export function formattedDate(input) {
  return input;
}

export default Ember.Handlebars.makeBoundHelper(formattedDate);
*/
